'use strict';
angular.module('cmmApp').run(['$rootScope', '$state', '$stateParams','$timeout','MenuConstant','uanalytics',
  function ( $rootScope,   $state,   $stateParams,$timeout,MenuConstant,uanalytics) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    MenuConstant[0]['menu'].push(uanalytics);
  }
])
.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'MODULE_CONFIG',
  function($stateProvider, $urlRouterProvider, $locationProvider,  MODULE_CONFIG) {
    var url_base_forcast="../apps/u-analytics-ui/";
    $locationProvider.html5Mode(true);
    $stateProvider

    .state('app.analytics', {
              abstract: false,
              url: 'reportAnalytics',
              templateUrl: url_base_forcast+'views/analytics.html',
              resolve: load([url_base_forcast+'css/u-forecast.css']),
              controller: 'analyticsController',
              controllerAs: 'analytics'
    }).state('app.infrastructure', {
              abstract: false,
              url: '/infrastructureAnalytics',
              templateUrl: url_base_forcast+'views/infrastructure.html',
              resolve: load([url_base_forcast+'css/u-forecast.css']),
              controller: 'infrastructureController',
              controllerAs : 'infrastructure'
    }).state('app.status', {
              abstract: false,
              url: '/statusAnalytics',
              templateUrl: url_base_forcast+'views/status.html',
              resolve: load([url_base_forcast+'css/u-forecast.css']),
              controller: 'statusController',
              controllerAs : 'status'
    }).state('app.timeToConfirm', {
              abstract: false,
              url: '/timeToConfirm',
              templateUrl: url_base_forcast+'views/confirm.html',
              resolve: load([url_base_forcast+'css/u-forecast.css']),
              controller: 'timeToConfirm',
              controllerAs : 'confirm'
    });

    function load(srcs, callback) {
      return {
        deps: ['$ocLazyLoad', '$q',
          function( $ocLazyLoad, $q ){
            var deferred = $q.defer();
            var promise  = false;
            srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
            if(!promise){
              promise = deferred.promise;
            }

            angular.forEach(srcs, function(src) {
              promise = promise.then( function(){
                angular.forEach(MODULE_CONFIG, function(module) {
                  if( module.name == src){
                    if(!module.module){
                      name = module.files;
                    }else{
                      name = module.name;
                    }
                  }else{
                    name = src;
                  }
                });

                return $ocLazyLoad.load(name);
              });
            });

            deferred.resolve();

            return callback ? promise.then(function(){ return callback(); }) : promise;
          }
        ]
      }
      return srcs;
    }
  }
]).constant("uanalytics", [{"title": "U-Analytics",
                            "icon":"mdi-action-face-unlock",
                            "count":'3',
                            "subMenu":[{
                              'title':'Asistencia Docente',
                              'url': "app.analytics",
                            },{
                              'title':'Uso infraestructura',
                              'url': "app.infrastructure",
                            },{
                              'title':'Estado eventos ',
                              'url': "app.status",
                            },{
                              'title':'Tiempo de confirmación ',
                              'url': "app.timeToConfirm",
                            }]
                          }]);