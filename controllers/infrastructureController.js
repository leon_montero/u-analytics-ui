/**
 * @name APP cmmApp
 * @autor jaime.salazar@u-planner.com
 * @description
 * # name controller --> infrastructureController
 * # As Controller --> infrastructure 
 * Controller associado a vista principal de reportes 2 uso infraestructura analytics
 */

(function(){
    'use strict';

    angular
    .module('cmmApp')
    .controller('infrastructureController', infrastructureController);

    infrastructureController.$inject = ['$scope', '$filter', '$http', 'analyticsServices'];

    function infrastructureController($scope, $filter, $http, analyticsServices){
        var vm = this;
        vm.loadReport = loadReport;
        vm.valuationDatePickerOpen = valuationDatePickerOpen;
        vm.valuationDatePickerIsOpen = false;
        vm.valuationDate = new Date();
        $scope.myModel={cam:'', edi:'', date:''};

        /////////////////////////////
        function valuationDatePickerOpen() {
            this.valuationDatePickerIsOpen = true;
        };

        ////// Catalogos
        analyticsServices.resourceReport.catalogue().$promise
        .then(function(data){
            $scope.catalogues = data;
            //console.log('catalogues', $scope.catalogues); 
        });

        function loadReport(){
            //notificationService.success( 'Cargando Data para dibujar tabla ...');
            $scope.dat = moment($scope.myModel.date).format("YYYYMMDD");
            $scope.myModel = {
                'cam' : $scope.myModel.cam,
                'edi' : $scope.myModel.edi,
                'dat':  $scope.dat
            };

            var model = {};
            //console.log('myModel', $scope.myModel)
            angular.copy($scope.myModel, model);

            $scope.panelReport = true;
            analyticsServices.resourceReport.recursos(model).$promise
            .then(function(data){
                console.log('data',data);
                $scope.gridOptions.data = data.data;
            });
        }

        $scope.gridOptions = {
            enableGridMenu: true,
            paginationPageSizes: [20, 50, 100],
            paginationPageSize: 20,
            exporterPdfDefaultStyle: {fontSize: 7},
            exporterPdfTableStyle: {margin: [5, 5, 5, 5]},
            exporterPdfTableHeaderStyle: {fontSize: 8, bold: true, italics: true, color: 'blue'},
            exporterPdfMaxGridWidth: 620,
            selectionRowHeaderWidth: 40,
            exporterPdfHeader: {
                text: "Uso infraestructura ",
                style: 'headerStyle'
            },
            exporterPdfCustomFormatter: function(docDefinition) {
                docDefinition.styles.headerStyle = {
                    fontSize: 15,
                    bold: true,
                    marginTop: 10,
                    marginLeft: 40,
                    marginBottom: 40
                };
                return docDefinition;
             }

            //  columnDefs: [ 

            // {
            //   field: 'modulo',
            //   displayName: 'Modulo',
            //   width: '12%',
            //   cellClass:'red',
            //   headerCellClass: 'red'
            // },
            // {
            //   field: 'AUD001 AUDITORIUM-268',
            //   displayName: 'AUD001 AUDITORIUM-268',
            //   width: '18%',
            //   headerCellClass: 'test'
            // },
            // {
            //   field: 'BOX401 SALON DE CLASES - BOX404 - 3',
            //   displayName: 'BOX401 SALON DE CLASES - BOX404 - 3',
            //   width: '18%',
            //   headerCellClass: 'test',
            //   cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
            //          return 'evento';
            //     }
            // },
            // {
            //   field: 'COM201 LAB. COMPUTACIÓN - 42',
            //   displayName: 'COM201 LAB. COMPUTACIÓN - 42',
            //   width: '18%',
            //   headerCellClass: 'test'
            // },
            // {
            //   field: 'COM202 LAB. COMPUTACIÓN - 46',
            //   displayName: 'COM202 LAB. COMPUTACIÓN - 46',
            //   width: '18%',
            //   headerCellClass: 'test'
            // },
            // {
            //   field: 'COM303 LAB. COMPUTADORES (ADVANCE) - 30',
            //   displayName: 'COM303 LAB. COMPUTADORES (ADVANCE) - 30',
            //   width: '18%',
            //   headerCellClass: 'test'
            // },
            // {
            //   field: 'SAL001 SALÓN DE CLASES - 60',
            //   displayName: 'SAL001 SALÓN DE CLASES - 60',
            //   width: '18%',
            //   headerCellClass: 'test'
            // },
            // {
            //   field: 'COM202 LAB. COMPUTACIÓN - 46',
            //   displayName: 'COM202 LAB. COMPUTACIÓN - 46',
            //   width: '18%',
            //   headerCellClass: 'test'
            // },
            // {
            //   field: 'SAL003 SALÓN DE CLASES (PIZARRA DIG) - 30',
            //   displayName: 'SAL003 SALÓN DE CLASES (PIZARRA DIG) - 30',
            //   width: '18%',
            //   headerCellClass: 'test'
            // },
            // {
            //   field: 'SAL005 SALÓN DE CLASES (PIZARRA DIG) - 30',
            //   displayName: 'SAL005 SALÓN DE CLASES (PIZARRA DIG) - 30',
            //   width: '18%',
            //   headerCellClass: 'test'
            // }

            // ]
        };

    }
})();
