/**
 * @name APP cmmApp
 * @autor jaime.salazar@u-planner.com
 * @description
 * # name controller --> timeToConfirm
 * # As Controller --> status 
 * Controller associado a vista principal de reportes tiempo de confirmacion
 */

(function(){
  'use strict';

  angular
  .module('cmmApp')
  .controller('timeToConfirm', timeToConfirm);

	timeToConfirm.$inject = ['$scope', '$http' , 'analyticsServices','$filter' , 'uiGridConstants','uiGridTreeViewConstants' ,'$modal' ,'uiGridGroupingConstants','$window'];

	function timeToConfirm($scope, $http , analyticsServices  , $filter , uiGridConstants , uiGridTreeViewConstants , $modal , uiGridGroupingConstants ,$window ){
        
        var vm = this;
        vm.viewPanel = viewPanel;
        vm.viewDetailAsistance = viewDetailAsistance;
        vm.hideDetailAsistance = hideDetailAsistance;
        var url_base_analytics="../apps/u-analytics-ui/";
        $scope.myModel={ date:'' };
       
        function viewPanel(){

            $scope.panelReport = true;
            vm.startDate = moment($scope.myModel.date.startDate._d).format("YYYYMMDD");
            vm.endDate = moment($scope.myModel.date.endDate._d).format("YYYYMMDD");
            $scope.myModel={
              'din':  vm.startDate,
              'dfi':  vm.endDate,   
            };
 
            var model = {};
            console.log('myModel',$scope.myModel)
            angular.copy($scope.myModel, model);
   
        
           analyticsServices.recursoReport.status(model).$promise
            .then(function(data){
             console.log('datajuice',data)
 
            $scope.gridDetalles.data = data.data.tabla1;

            //grafico confirmacion
           
                $scope.chartCampus ={
                    "options": {
                        "chart": {
                            "type": "column"
                        },
                    },
                    title: {
                    text: 'Tiempos confirmacion '
                        },
                    subtitle :{
                    text: 'Por Sedes'  
                    },
                    xAxis: {
                    categories: data.data.grafico1p.categoria,
                    labels: {
                                rotation: -60,
                               style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                           }
                    },
                    yAxis: {
                    min: 0,
                    title: {
                        text: ''
                       }
                   },
                   series: data.data.grafico1p.series
                };
                 
            });
        }    
 
        //function que despliega tabla detalle asistencia docentes
        function viewDetailAsistance(){
          $scope.detailsGrid = true ;
          $scope.viewDetailsButton = true ;
          $scope.hideDetails = true ;
        }
 
        //function que oculta tabla detalle asistencia docentes
        function hideDetailAsistance(){
          $scope.detailsGrid = false ;
          $scope.viewDetailsButton = false ;
          $scope.hideDetails = false ;
        }
 
        
 
        //Grilla detalle Eventos  
        $scope.gridDetalles = {
            enableSorting: true,
            enableFiltering: true,
            enableGridMenu: true,
            showColumnFooter: true,
           
            exporterPdfHeader: {
                    text: "Eventos ",
                    style: 'headerStyle'
                },
                exporterPdfCustomFormatter: function(docDefinition) {
                    docDefinition.styles.headerStyle = {
                        fontSize: 22,
                        bold: true,
                        marginTop: 10,
                        marginLeft: 40,
                        marginBottom: 20
                    };
                    return docDefinition;
                },
 
                columnDefs: [
 
            {
              field: 'des_campus',
              displayName: 'Campus',
              enableCellEdit: true,
              enableSorting: false,
              width: '35%',
              aggregationType: "Total General"
            },
            {
              field: 'activo',
              enableFiltering: false,
              displayName: 'Activo',
              enableCellEdit: true,
              width: '17%',
              aggregationType: uiGridConstants.aggregationTypes.sum,
              cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                if (grid.getCellValue(row,col) === 0) {
                     return 'menores';
                   }
                }
           },
           {
              field: 'realizado',
              enableFiltering: false,
              displayName: 'Realizado',
              enableCellEdit: true,
              width: '17%',
              aggregationType: uiGridConstants.aggregationTypes.sum
           },
           {
              field: 'suspendido',
              enableFiltering: false,
              displayName: 'Suspendido',
              enableCellEdit: true,
              width: '17%',
              cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                 if (grid.getCellValue(row,col) === 719) {
                      return 'menores';
                   }else{
 
                     return 'suspendido';
 
                   }
                 },
              aggregationType: uiGridConstants.aggregationTypes.sum
           },
           {
              field: 'total',
              enableFiltering: false,
              displayName: 'Total',
              enableCellEdit: true,
              width: '18%',
              aggregationType: uiGridConstants.aggregationTypes.sum
        }
        ]
         
        };
 
       
    }
 
})();