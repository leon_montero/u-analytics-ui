/**
 * @name APP cmmApp
 * @autor jaime.salazar@u-planner.com
 * @description
 * # name controller --> detailsTeacherController
 * # As Controller --> detailsTeacher
 * controllador de modal que muestra en detalle la "asistencia" de "docente" seleccionado en grilla detalle
 */

(function(){
    'use strict';

    angular
        .module('cmmApp')
        .controller('detailsTeacherController', detailsTeacherController);

    detailsTeacherController.$inject = ['$scope', '$filter', '$http','$modalInstance','analyticsServices'];

    function detailsTeacherController($scope, $filter, $http, $modalInstance,analyticsServices){
        var vm = this;
        vm.cancel = cancel;
        vm.name = $scope.detailsModules.name;
        vm.id = $scope.detailsModules.id;
       
        /////////////////////////////

        function cancel() {
            $modalInstance.dismiss('cancel');

        }

        analyticsServices.detailsAsistance.details($scope.model).$promise
        .then(function(data){

        $scope.gridOptions.data = data.data;          

        });

        console.log('modelo de sugerencia details',$scope.model);

        $scope.gridOptions ={
            enableGridMenu: true,
            exporterPdfDefaultStyle: {fontSize: 7},
            exporterPdfTableStyle: {margin: [5, 5, 5, 5]},
            exporterPdfTableHeaderStyle: {fontSize: 8, bold: true, italics: true, color: 'blue'},
            exporterPdfMaxGridWidth: 620,
            exporterPdfHeader: {
                text: "Uso infraestrutura ",
                style: 'headerStyle'
            },
            exporterPdfCustomFormatter: function(docDefinition) {
                docDefinition.styles.headerStyle = {
                    fontSize: 15,
                    bold: true,
                    marginTop: 10,
                    marginLeft: 40,
                    marginBottom: 40
                };
                return docDefinition;
            },
                columnDefs : [{
                    name: 'docente',
                    field: 'docente',
                    width: '20%'
                },{
                    name: 'asistencia',
                    field: 'asistencia',
                    width: '15%'
                },{ 
                    name: 'campus',
                    field: 'campus',
                    width: '18%'
                },{
                    name: 'periodo_academico',
                    field: 'periodo_academico',
                    width: '15%'
                },{
                    name: 'facultad',
                    field: 'facultad',
                    width: '20%'
                },{
                    name: 'fecha',
                    field: 'fecha',
                    width: '12%'
                },{
                    name: 'modulo',
                    field: 'modulo',
                    width: '25%'
                },{
                    name: 'comentario_pantalla',
                    field: 'comentario_pantalla',
                    width: '10%'
                }, {
                    name: 'docente_reemplazo',
                    field: 'docente_reemplazo',
                    width: '20%'
                },{
                    name: 'minutos_atraso',
                    field: 'minutos_atraso',
                    width: '20%'
                },{
                    name: 'edificio',
                    field: 'edificio',
                    width: '20%'
                },{
                    name: 'salon',
                    field: 'salon',
                    width: '15%'
                },{
                    name: 'curso',
                    field: 'curso',
                    width: '16%'
                },{

                    name: 'seccion',
                    field: 'seccion',
                    width: '22%'
                }]
        };

    }
})();