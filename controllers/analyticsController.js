/**
 * @name APP cmmApp
 * @autor jaime.salazar@u-planner.com
 * @description
 * # name controller --> analyticsController
 * # As Controller --> analytics 
 * Controller associado a vista principal de reportes asistencia docentes analitics
 */

(function(){
	'use strict';

	angular
		.module('cmmApp')
		.controller('analyticsController', analyticsController);

	analyticsController.$inject = ['$scope', '$http' , 'analyticsServices','$filter' , 'uiGridConstants','uiGridTreeViewConstants' ,'$modal' ,'uiGridGroupingConstants','$window'];

	function analyticsController($scope, $http , analyticsServices  , $filter , uiGridConstants , uiGridTreeViewConstants , $modal , uiGridGroupingConstants ,$window ){

		var vm = this;
        var url_base_analytics="../apps/u-analytics-ui/";
		vm.viewModule = viewModule;
        vm.viewPanel = viewPanel;
		vm.viewDetailAsistance = viewDetailAsistance;
		vm.hideDetailAsistance = hideDetailAsistance;
        vm.toggleCustom = toggleCustom;
        $scope.myModel={ date:'' };
        $scope.hideGrid = true;
        $scope.gridDetalles = { };

        function viewPanel(){
            $scope.panelReport = true;
            $scope.startDate = moment($scope.myModel.date.startDate._d).format("YYYYMMDD");
            $scope.endDate = moment($scope.myModel.date.endDate._d).format("YYYYMMDD");

            $scope.myModel={
                'cam' : $scope.myModel.cam,
                'per' : $scope.myModel.per,
                'fac' : $scope.myModel.fac,
                'din':  $scope.startDate,
                'dfi':  $scope.endDate,
            };

            var model = {};
            console.log('myModel',$scope.myModel)
            angular.copy($scope.myModel, model);

            
            analyticsServices.detailsAsistance.tableDetails(model).$promise
            .then(function(data){
            if(!angular.isUndefined(data.data.tabla3)){
                var docentes =  data.data.tabla3;
                $scope.gridDetalles.data = [];
                writeoutNode( docentes, 0, $scope.gridDetalles.data); 
              }
            });
         
    
           //analyticsServices.callFilters(model)
           analyticsServices.detailsAsistance.asistencia(model).$promise
            .then(function(data){
 
            $scope.gridCampus.data = data.data.tabla1;
            $scope.gridFacultad.data = data.data.tabla2;

            //grafico muestra asistencia docentes por campus
                $scope.chartCampus ={
                    "options": {
                        "chart": {
                            "type": "column"
                        },
                    },
                    title: {
                    text: 'Asistencia Docentes'
                        },
                    subtitle :{
                    text: 'Por Campus'  
                    },
                    xAxis: {
                    categories: data.data.grafico1p.categories
                    },
                    yAxis: {
                    min: 0,
                    title: {
                        text: ''
                       }
                   },
                   series: data.data.grafico1p.series
                }; 
                
                //grafico muestra asistencia docentes por campus
                $scope.chartFacultad ={
                    "options": {
                    "chart": {
                        "type": "column",
                          height: 700
                        },
                    },
                    title: {
                    text: 'Asistencia Docentes'
                    },
                    subtitle :{
                    text: 'Por Facultad'  
                    },
                    xAxis: {
                    categories: data.data.grafico2p.categories,
                        labels: {
                               rotation: -60,
                               style: {
                                   fontSize: '13px',
                                   fontFamily: 'Verdana, sans-serif'
                               }
                           }
                        },
                    yAxis: {
                    min: 0,
                    title: {
                        text: ''
                       }
                    },
                    series: data.data.grafico2p.series
                }   

            });
        }

        $scope.custom = true;
        function toggleCustom() {
            $scope.custom = $scope.custom === false ? true: false;
        };

        //function que despliega tabla detalle asistencia docentes
        function viewDetailAsistance(){
          $scope.detailsGrid = true;
          $scope.hideDetails = true;
        }

        //function que oculta tabla detalle asistencia docentes
        function hideDetailAsistance(){
          $scope.hideDetails = false;
          $scope.detailsGrid = false;
        }

        //function que muestra modal con detalle de docente en especifico
        function viewModule (row,day,attends){

            if(!angular.isUndefined(row.entity.id && row.entity.name)){
                $scope.idDocente = row.entity.id;
                $scope.nameDocente = row.entity.name;     
              }else {
                $scope.idDocente = row.treeNode.parentRow.entity.id;
                $scope.nameDocente = row.treeNode.parentRow.entity.name;
              }
             

            vm.dia=day;
            vm.asiste=attends;

            $scope.details={

                "id":$scope.idDocente,
                "dia":vm.dia,
                "asist": vm.asiste,
                'cam' : $scope.myModel.cam,
                'per' : $scope.myModel.per,
                'fac' : $scope.myModel.fac,
                'din':  $scope.startDate,
                'dfi':  $scope.endDate,

            };

            console.log('modelo',$scope.details);
            console.log('modal Detalle Detalle',row.entity);

           $scope.detailsModules = row.entity;
           $scope.model = $scope.details;

               var modalInstance  = $modal.open({
                templateUrl: url_base_analytics+'views/modalReport/teacherDetails.html',
                animation: true,
                scope: $scope,
                controller: 'detailsTeacherController',
                controllerAs: 'detailsTeacher',
                size: 'lg'
            });              
        }

         //////multiselect Catalogos

        // var cataloguePromise = analyticsServices.callCatalogue();
        // cataloguePromise.then(function(response) {
        //     $scope.catalogues = {
        //         data: response.data
        //     };
        // });

        analyticsServices.callCatalogue.catalogue().$promise
            .then(function(data){
                $scope.catalogues = data;
              
               console.log('catalogues',$scope.catalogues); 
            });

		
       //Grilla asistencia docente por campus
        $scope.gridCampus = {
            enableFiltering: true,
            showTreeExpandNoChildren: true,
            enableGridMenu: true,
            showColumnFooter: true,
            exporterPdfHeader: {
                text: "Asistencia Docentes",
                style: 'headerStyle'
            },
            exporterPdfCustomFormatter: function(docDefinition) {
                docDefinition.styles.headerStyle = {
                    fontSize: 22,
                    bold: true,
                    marginTop: 10,
                    marginLeft: 40,
                    marginBottom: 20
                };
                return docDefinition;
            },

            columnDefs: [

            {
              field: 'campus',
              enableFiltering: false,
              displayName: 'Campus',
              enableCellEdit: true,
              enableSorting: false,
              width: '35%'
            },
            {
              field: 'noregistrado',
              enableFiltering: false,
              displayName: 'No registrado',
              enableCellEdit: true,
              width: '25%',
              aggregationType: uiGridConstants.aggregationTypes.sum
           },
           {
              field: 'inasistente',
              enableFiltering: false,
              displayName: 'No Asiste',
              enableCellEdit: true,
              width: '20%',
              aggregationType: uiGridConstants.aggregationTypes.sum
           },
           {
              field: 'asistente',
              enableFiltering: false,
              displayName: 'Asiste',
              enableCellEdit: true,
              width: '15%',
              aggregationType: uiGridConstants.aggregationTypes.sum
           },
           {
              field: 'total',
              enableFiltering: false,
              displayName: 'Total',
              enableCellEdit: true,
              width: '15%',
              aggregationType: uiGridConstants.aggregationTypes.sum
        }
        ]
        };

        //Grilla asistencia docente por Facultad
        $scope.gridFacultad = {
            enableFiltering: true,
            showTreeExpandNoChildren: true,
            enableGridMenu: true,
            showColumnFooter: true,
                exporterPdfHeader: {
                    text: "Asistencia Docentes",
                    style: 'headerStyle'
                },
                exporterPdfCustomFormatter: function(docDefinition){
                    docDefinition.styles.headerStyle = {
                        fontSize: 22,
                        bold: true,
                        marginTop: 10,
                        marginLeft: 40,
                        marginBottom: 20
                    };
                    return docDefinition;
                },

                columnDefs: [

                {
                    field: 'facultad',
                    displayName: 'Facultad',
                    enableFiltering: false,
                    enableCellEdit: true,
                    enableSorting: false,
                    width: '45%'
                },
                {
                    field: 'noregistrado',
                    enableFiltering: false,
                    displayName: 'No registra',
                    enableCellEdit: true,
                    width: '15%',
                    aggregationType: uiGridConstants.aggregationTypes.sum
                },
                {
                    field: 'inasistente',
                    enableFiltering: false,
                    displayName: 'No Asiste',
                    enableCellEdit: true,
                    width: '15%',
                    aggregationType: uiGridConstants.aggregationTypes.sum

                },
                {
                    field: 'asistente',
                    enableFiltering: false,
                    displayName: 'Asiste',
                    enableCellEdit: true,
                    width: '12%',
                    aggregationType: uiGridConstants.aggregationTypes.sum
                },
                
                {
                    field: 'total',
                    enableFiltering: false,
                    displayName: 'Total',
                    enableCellEdit: true,
                    width: '12%',
                    aggregationType: uiGridConstants.aggregationTypes.sum
                }
                ]
        };

        //Grilla detalle asistencia docente   
        $scope.gridDetalles = {
            enableSorting: true,
            enableFiltering: true,
            showTreeExpandNoChildren: true,
            enableGridMenu: true,
           
            exporterPdfHeader: {
                    text: "Asistencia Docentes",
                    style: 'headerStyle'
                },
                exporterPdfCustomFormatter: function(docDefinition) {
                    docDefinition.styles.headerStyle = {
                        fontSize: 22,
                        bold: true,
                        marginTop: 10,
                        marginLeft: 40,
                        marginBottom: 20
                    };
                    return docDefinition;
                },
         
            columnDefs: [
      
            {
                field: 'name',
                displayName: 'Docente',
                enableCellEdit: true,
                enableSorting: false,
                width: '35%'
            },
            {
                field: 'lunes',
                enableFiltering: false,
                displayName: 'LU',
                enableCellEdit: true,
                width: '5%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,2,0)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.lunes}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                }             
            },
            {
                field: 'martes',
                enableFiltering: false,
                displayName: 'MA',
                enableCellEdit: true,
                width: '5%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,3,0)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.martes}}</a>' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                }             
            },
            {
                field: 'miercoles',
                displayName: 'MI',
                enableFiltering: false,
                enableCellEdit: true,
                width: '5%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,4,0)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.miercoles}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 
            },
            {
                field: 'jueves',
                displayName: 'JU',
                enableFiltering: false,
                enableCellEdit: true,
                width: '5%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,5,0)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.jueves}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 
            },
            {
                field: 'viernes',
                displayName: 'VI',
                enableFiltering: false,
                enableCellEdit: true,
                width: '5%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,6,0)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.viernes}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 
            },
            {
                field: 'sabado',
                displayName: 'SA',
                enableFiltering: false,
                enableCellEdit: true,
                width: '5%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,7,0)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.sabado}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 
            },
            {
                field: 'asiste',
                displayName: 'Asiste',
                enableFiltering: false,
                enableCellEdit: true,
                width: '8%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,0,1)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.asiste}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 
            },
            {
                field: 'noasiste',
                displayName: 'No Asiste',
                enableFiltering: false,
                enableCellEdit: true,
                width: '8%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,0,2)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.noasiste}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 
            },
            {
                field: 'noregistrado',
                displayName: 'No Registrado',
                enableFiltering: false,
                enableCellEdit: true,
                width: '8%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,0,3)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.noregistrado}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 
            },
            {
                field: 'total',
                displayName: 'Total',
                enableFiltering: false,
                enableCellEdit: true,
                width: '8%',
                cellTemplate: '<div>' +
                            '<a ng-click="grid.appScope.analytics.viewModule(row,0,0)" title="Ver Modulo" tooltip-html-unsafe="Ver detalle asistencia Docente"> &nbsp; {{row.entity.total}}</a> ' +
                            '</div>',
                cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex) {
                  if (grid.getCellValue(row,col) === 0) {
                    return 'visibility';
                  }
                } 

            }
     
           ]
        };

        // function que permite mostrar hijos en grilla

        var writeoutNode = function( childArray, currentLevel, dataArray ){
          childArray.forEach( function( childNode ){
            if(!angular.isUndefined(childNode.children)){
            if ( childNode.children.length > 0 ){
              childNode.$$treeLevel = currentLevel;
            }}
            dataArray.push( childNode );
            if(!angular.isUndefined(childNode.children)){
            writeoutNode( childNode.children, currentLevel + 1, dataArray);
          }});
        };
    }

})();