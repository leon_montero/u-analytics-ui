/**
 * @name cmmApp
 * @autor jaime.salazar@u-planner.com
 * @description
 * # cmmApp
 * Factory que realiza llamadas a servicios para module analytics
 */

(function() {
  'use strict';
  angular
  .module('cmmApp')
  .factory('analyticsServices', analyticsServices);

  analyticsServices.$inject = ['$q', '$http', '$location', '$window', '$resource'];

  function analyticsServices($q, $http, $location, $window, $resource) {

    // llamadas servicios catalogo general , periodos , facultades
    var callCatalogue = $resource('http://booking.u-planner.com:8000/asistencia/:controller',{},{

      catalogue :{
        method:'GET',
        params : {
          controller: 'get_catalogos.php'
        }
      }
    })

    // llamadas servicios para Asistencia
    var detailsAsistance = $resource('http://localhost:8000/u_booking/asistencia/:controller',{},{
      asistencia :{
        method:'POST',
        params : {
          controller: 'get_dash_asistencia.php'
        }
      },

      details :{
        method:'POST',
        params : {
          controller: 'get_tabla_detalle_asistencia.php'
        }
      },

      tableDetails :{
        method:'POST',
        params : {
          controller: 'get_tabla_asistencia.php'
        }
      }
    })

    // llamadas servicios para Recursos
    var resourceReport = $resource('http://booking.u-planner.com:8000/recursos/:controller',{},{
        
      catalogue :{
        method:'GET',
        params : {
          controller: 'get_catalogos.php'
        }
      },

      recursos :{
        method:'POST',
        params : {
          controller: 'get_eventos_recursos.php'
        }
      }
    })

    // llamadas servicios para eventos
    var eventos = $resource('http://booking.u-planner.com:8000/eventos/:controller',{},{

      event :{
        method:'POST',
        params : {
          controller: 'get_eventos_extra.php'
        }
      }
    })

    return {
      callCatalogue: callCatalogue,
      detailsAsistance: detailsAsistance,
      resourceReport: resourceReport,
      eventos: eventos
    };

  }
})();